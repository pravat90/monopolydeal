package monopolydeal_test;

import monopolydeal.*;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BancoTest {
	Banco banco;
	Carta c1, c2, c3, c4;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		this.banco = new Banco();
		this.c1 = new CartaDinero(1); // Valor: 1
		this.c2 = new CartaPropiedad(MonopolyDeal.grupoCeleste); // Valor: 1
		this.c3 = new CartaAccion(MonopolyDeal.accionHotel); // Valor: 4
		this.c4 = new CartaComodin(MonopolyDeal.comodinMulti); // Valor: 0 <No se puede guardar>
	}

	@Test
	public void testAddRemove() {
		assertEquals(true, this.banco.bancoVacio());
		assertEquals(true, this.banco.addCarta(c1));
		assertEquals(true, this.banco.addCarta(c2));
		assertEquals(true, this.banco.addCarta(c3));
		assertEquals(false, this.banco.addCarta(c4));
		assertEquals(6, this.banco.getValor());
		
		assertEquals(false, this.banco.bancoVacio());
		assertEquals(3, this.banco.getQCartas());
		assertEquals(c1, this.banco.getCarta(0));
		assertEquals(5, this.banco.getValor());
	}
}
