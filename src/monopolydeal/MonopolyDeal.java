package monopolydeal;

import java.util.ArrayList;

public final class MonopolyDeal extends MonopolyDealRules {
	
    private static MonopolyDeal monopolyDeal = new MonopolyDeal();
    
	private ArrayList<Jugador> jugadores;
	private int jugadorActual;
	private int estado;
	private Mazo mazoPrincipal;
	private Mazo mazoDescarte;
	
	private MonopolyDeal (){
		this.mazoPrincipal = new Mazo();
		this.mazoDescarte = new Mazo();
		this.jugadores = new ArrayList<Jugador>();
		this.estado = MonopolyDeal.estadoEspera;
	}
	
	public static MonopolyDeal getInstance(){
		return MonopolyDeal.monopolyDeal;
	}
	
	/**
	 * Obtiene el estado de partida
	 * @return Estado de partida
	 */
	public int getEstado() {
		return this.estado;
	}
	
	/**
	 * Inicia la partida
	 * @return TRUE: Partida Iniciada
	 */
	public boolean inicioPartida (){
		if ((this.estado == MonopolyDeal.estadoEspera) && this.jugadoresMinimo()){
			this.estado = MonopolyDeal.estadoJugando;
			this.jugadorActual = 0;
			this.mazoPrincipal.limpiar();
			this.mazoDescarte.limpiar();
			this.mazoPrincipal.nuevo();
			this.mazoPrincipal.mezclar();
			for (int i = 0; i < this.qJugadores(); i++){
				Jugador jx = this.jugadores.get(i);
				for (int j = 0; j < 5; j++){
					jx.addCarta(this.mazoPrincipal.getCarta());
				}
				
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Intercambia mazo de descarte
	 * @return TRUE: Mazos Intercambiados
	 */
	public boolean intercambiarMazo (){
		if (this.mazoPrincipal.mazoVacio() && !this.mazoDescarte.mazoVacio()){
			this.mazoPrincipal = this.mazoDescarte;
			this.mazoPrincipal.mezclar();
			this.mazoDescarte.limpiar();
			return true;
		}
		return false;
	}
	
	/**
	 * Finaliza la partida
	 * @return TRUE: Partida Finalizada
	 */
	public boolean finalPartida (){
		if (this.estado == MonopolyDeal.estadoJugando){
			this.mazoPrincipal.limpiar();
			this.mazoDescarte.limpiar();
			this.jugadores = new ArrayList<Jugador>();
			this.estado = MonopolyDeal.estadoEspera;
			return true;
		}
		return false;
	}

	/**
	 * Obtiene la cantidad de jugadores agregados
	 * @return Cantidad jugadores
	 */
	public int qJugadores (){
		return this.jugadores.size();
	}
	
	/**
	 * Verifica si no hay jugadores
	 * @return TRUE: No hay jugadores
	 */
	public boolean jugadoresVacio (){
		return this.jugadores.isEmpty();
	}
	
	/**
	 * Verifica si hay cantidad minima de jugadores
	 * @return TRUE: Minimo jugadores
	 */
	public boolean jugadoresMinimo (){
		return this.qJugadores() >= MonopolyDeal.minJugadores;
	}
	
	/**
	 * Verifica si hay cantidad maxima de jugadores
	 * @return TRUE: Maximo jugadores
	 */
	public boolean jugadoresLleno (){
		return this.qJugadores() == MonopolyDeal.maxJugadores;
	}
	
	/**
	 * Agrega jugador
	 * @param nombre Nombre de jugador
	 * @return Jugador configurado
	 */
	public boolean addJugador (String nombre){
		if (!this.jugadoresLleno()){
			Jugador j = new Jugador (this.qJugadores(), nombre);
			this.jugadores.add(j);
			return true;
		}
		return false;
	}
	
	/**
	 * Obtiene el jugador especificado
	 * @param n Indice de jugador
	 * @return Jugador especifico
	 */
	public Jugador getJugador (int n){
		return this.jugadores.get(n);
	}
	
	/**
	 * Actualiza jugador especifico
	 * @param n Indice de jugador
	 * @param j Jugador a guardar
	 */
	public void setJugador (int n, Jugador j){
		this.jugadores.set(n, j);
	}
	
	/**
	 * Obtiene el jugador actual de la ronda
	 * @return Indice de jugador
	 */
	public int getJugadorActual() {
		return this.jugadorActual;
	}
	
	/**
	 * Obtiene el siguiente jugador de la ronda
	 * @return Indice de jugador
	 */
	public int getJugadorSiguiente(){
		return this.jugadorActual + 1;
	}
	
	
	/**
	 * Inicia turno del jugador actual
	 */
	public void inicioTurno (){
		Jugador jx = this.jugadores.get(this.jugadorActual);
		for (int i = 0; i < 2; i++){
			jx.addCarta(this.getCarta());
		}
		this.jugadores.set(this.jugadorActual, jx);
	}
	
	/**
	 * Termina el turno actual
	 */
	public void finalTurno (){
		this.jugadorActual = this.getJugadorSiguiente();
	}

	public void descatarCarta(Carta c) {
		this.mazoDescarte.addCarta(c);
	}
	
	public String[] getList (){
		String[] jugadores = new String[this.jugadores.size()];
		for (int i = 0; i < this.jugadores.size(); i++){
			jugadores[i] = this.jugadores.get(i).toString();
		}
		return jugadores;
	}
	
	public Carta getCarta (){
		if (this.mazoPrincipal.mazoVacio()){
			this.intercambiarMazo();
		}
		return this.mazoPrincipal.getCarta();
	}
	
	public int getGanador (){
		int g = -1;
		for (int i = 0; i < this.jugadores.size(); i++){
			if (this.getJugador(i).gruposNecesarios()){
				g = i;
			}
		}
		return g;
	}
	
	public boolean hayGanador (){
		return this.getGanador() != -1;
	}
}
