package monopolydeal; 

import java.util.ArrayList;
import java.util.Random;

public class Mazo {
    final static int maxCartas = 106;
    
    private ArrayList<Carta> mazo;
    private Random aleatorio;
    
    public Mazo (){
        this.aleatorio = new Random();
        this.limpiar();
    }
    
    public void nuevo (){
    	// -> Cartas Dinero <-
    	for (int i = 0; i < 6; i++){ this.mazo.add(new CartaDinero(1)); }
    	for (int i = 0; i < 5; i++){ this.mazo.add(new CartaDinero(2)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaDinero(3)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaDinero(4)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaDinero(5)); }
    	for (int i = 0; i < 1; i++){ this.mazo.add(new CartaDinero(10)); }
    	// -> Cartas Renta <-
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaRenta(MonopolyDeal.rentaCelesteCafe)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaRenta(MonopolyDeal.rentaRosaNaranja)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaRenta(MonopolyDeal.rentaRojoAmarillo)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaRenta(MonopolyDeal.rentaAzulVerde)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaRenta(MonopolyDeal.rentaFerrocarrilServicio)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaRenta(MonopolyDeal.rentaMulti)); }
    	// -> Cartas Comodines <-
    	for (int i = 0; i < 1; i++){ this.mazo.add(new CartaComodin(MonopolyDeal.comodinCelesteCafe)); }
    	for (int i = 0; i < 1; i++){ this.mazo.add(new CartaComodin(MonopolyDeal.comodinCelesteFerrocarril)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaComodin(MonopolyDeal.comodinRosaNaranja)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaComodin(MonopolyDeal.comodinRojoAmarillo)); }
    	for (int i = 0; i < 1; i++){ this.mazo.add(new CartaComodin(MonopolyDeal.comodinAzulVerde)); }
    	for (int i = 0; i < 1; i++){ this.mazo.add(new CartaComodin(MonopolyDeal.comodinVerdeFerrocarril)); }
    	for (int i = 0; i < 1; i++){ this.mazo.add(new CartaComodin(MonopolyDeal.comodinFerrocarrilServicio)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaComodin(MonopolyDeal.comodinMulti)); }
    	// -> Cartas Accion <-
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionCumpleanios)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionCasa)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionHotel)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionCancelar)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionCobroDeuda)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionDobleRenta)); }
    	for (int i = 0; i < 10; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionAccesoLibre)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionTratoForzoso)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionTratoVencido)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaAccion(MonopolyDeal.accionTratoHecho)); }
    	// -> Cartas Propiedad <-
    	CartaPropiedad.reset();
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoCafe)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoCeleste)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoRosa)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoNaranja)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoRojo)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoAmarillo)); }
    	for (int i = 0; i < 3; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoVerde)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoAzul)); }
    	for (int i = 0; i < 4; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoFerrocarril)); }
    	for (int i = 0; i < 2; i++){ this.mazo.add(new CartaPropiedad(MonopolyDeal.grupoServicio)); }
    }
    
    public void limpiar (){
        this.mazo = new ArrayList<Carta>();
    }
    
    public void mezclar (){
        Carta c;
        int r;
        for (int i = 0; i < this.getQCartas(); i++){
            c = this.mazo.get(i);
            r = this.aleatorio.nextInt(this.getQCartas());
            this.mazo.set(i, this.mazo.get(r));
            this.mazo.set(r, c);
        }
    }
    
    public boolean mazoVacio (){
    	return this.mazo.isEmpty();
    }
    
    public boolean mazoLleno (){
        return this.getQCartas() == Mazo.maxCartas;
    }
    
    public int getQCartas (){
    	return this.mazo.size();
    }
    
    public void addCarta (Carta c){
        if (!this.mazoLleno() && this.cartaValida(c)){
            this.mazo.add(c);
        }
    }
    
    public Carta getCarta (){
        if (!this.mazoVacio()){
            return this.mazo.remove(this.getQCartas() - 1);
        }
        return null;
    }
    
    public boolean cartaValida (Carta c){
        return c != null;
    }

}
