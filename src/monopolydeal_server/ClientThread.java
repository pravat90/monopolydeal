package monopolydeal_server;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import monopolydeal.*;

public class ClientThread extends Thread{
	private MonopolyDeal monopolyDeal;
	
	private Socket clientSocket;
	private int clientId;
	private String clientName;
	private Exception lastError = null;
	
	private ObjectInputStream clientIn;
	private ObjectOutputStream serverOut;
	
	public ClientThread (Socket clientSocket){
		try {
			this.clientSocket = clientSocket;
			this.clientIn = new ObjectInputStream(this.clientSocket.getInputStream());
			this.clientName = this.clientIn.readUTF();
			this.serverOut = new ObjectOutputStream(this.clientSocket.getOutputStream());
			this.serverOut.write(this.clientId);
			this.monopolyDeal = MonopolyDeal.getInstance();
		} catch (Exception e){
			this.lastError = e;
		}
	}
	
	public Exception getError (){
		return this.lastError;
	}
	
	@Override
	public void run(){
		while (monopolyDeal.getEstado() == MonopolyDeal.estadoJugando){
			try {
				String command = clientIn.readUTF();
				if (command != null){
					String[] cmd = ServerCommands.splitCommand(command);
					Jugador j;
					Carta c;
					int n;
					boolean f = true;
					switch (cmd[0]){
						case ServerCommands.cmdAddJugador:
							int id = this.monopolyDeal.qJugadores();
							if (!this.monopolyDeal.addJugador(cmd[1])){
								f = false;
							}
							this.clientId =  - 1;
							break;
					
						case ServerCommands.cmdAddPropiedad:
							j = this.monopolyDeal.getJugador(this.clientId);
							c = j.getCarta(Integer.getInteger(cmd[1]));
							n = c.getGrupo();
							if (n == -1 || !j.addPropiedad(n, c)){
								j.addCarta(c);
								f = false;
							}
							this.monopolyDeal.setJugador(this.clientId, j);
							break;
							
						case ServerCommands.cmdAddComodin:
							j = this.monopolyDeal.getJugador(this.clientId);
							c = j.getCarta(Integer.getInteger(cmd[1]));
							n = Integer.getInteger(cmd[2]);
							if (n == -1 || !j.addPropiedad(n, c)){
								j.addCarta(c);
								f = false;
							}
							this.monopolyDeal.setJugador(this.clientId, j);
							break;
					
						case ServerCommands.cmdSaveCarta:
							j = this.monopolyDeal.getJugador(this.clientId);
							c = j.getCarta(Integer.getInteger(cmd[1]));
							if (!j.addDinero(c)){
								j.addCarta(c);
								f = false;
							}
							this.monopolyDeal.setJugador(this.clientId, j);
							break;
							
						case ServerCommands.cmdDumpCarta:
							j = this.monopolyDeal.getJugador(this.clientId);
							c = j.getCarta(Integer.getInteger(cmd[1]));
							this.monopolyDeal.descatarCarta(c);
							this.monopolyDeal.setJugador(this.clientId, j);
							break;
							
						case ServerCommands.cmdEndTurno:
							this.monopolyDeal.finalTurno();
							break;
							
						default:
							f = false;
							break;
					}
					this.serverOut.writeBoolean(f);
					j = this.monopolyDeal.getJugador(Integer.getInteger(cmd[1]));
					this.serverOut.writeObject(j);
				}
			} catch (Exception e){
				this.lastError = e;
			}
		}
	}
}
