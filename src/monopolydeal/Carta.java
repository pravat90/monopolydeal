package monopolydeal; 

import java.io.Serializable;

abstract public class Carta implements Serializable {
	private static final long serialVersionUID = -7475242348940139076L;
	
	protected String nombre;
    protected int tipo;
    protected int valor;
    
    public String getNombre (){
        return this.nombre;
    }
    
    public int getTipo (){
        return this.tipo;
    }
    
    public int getValor (){
        return this.valor;
    }
    
    public int getGrupo (){
    	return -1;
    };
    
    public boolean checkGrupo (int grupo){
    	return false;
    };
    
    public int getAccion (){
    	return -1;
    };
}
