package monopolydeal;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.ListIterator;

public class Banco implements Serializable {
	private static final long serialVersionUID = 3587176542827644165L;
	
	private ArrayList<Carta> cartas;
	
	public Banco (){
		this.cartas = new ArrayList<Carta>();
	}

	public boolean bancoVacio (){
		return this.cartas.isEmpty();
	}

	public boolean addCarta (Carta c){
		if (c != null){
			if ((c.getTipo() != MonopolyDeal.cartaComodin) && (c.getGrupo() != MonopolyDeal.comodinMulti)){
				return this.cartas.add(c);
			}
		}
		return false;
	}
	
	public int getQCartas (){
		return this.cartas.size();
	}
	
	public int getValor (){
		int valor = 0;
		ListIterator<Carta> li = this.cartas.listIterator();
		while (li.nextIndex() < this.cartas.size()){
			Carta c = li.next();
			valor += c.getValor();
		}
		return valor;
	}
	
	public Carta getCarta (int indice){
		if ((indice >= 0) && (indice < this.cartas.size())){
			return this.cartas.remove(indice);
		}
		return null;
	}

	public String[] getList() {
		if (!this.bancoVacio()){
			ArrayList<String> cartas = new ArrayList<String>();
			for (int i = 0; i < this.cartas.size(); i++){
				cartas.add(this.cartas.get(i).toString());
			}
			cartas.add("- Dinero: " + this.getValor() + "M");
			return cartas.toArray(new String[0]);
		}
		return null;
	}
}
