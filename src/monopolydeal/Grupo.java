package monopolydeal;

import java.io.Serializable;
import java.util.ArrayList;

public class Grupo implements Serializable {
	private static final long serialVersionUID = -8914188091204352014L;
	
	private int color;
    private int nivel1 = 0;
    private int nivel2 = 0;
    private int nivel3 = 0;
    private int nivel4 = 0;
    private ArrayList<Carta> propiedades;
    private int maxPropiedades;
    private Carta casa = null;
    private Carta hotel = null;
    
    public Grupo (int color){
    	switch (color){
    		case MonopolyDeal.grupoCafe:
    			this.nivel1 = 1;
    			this.nivel2 = 2;
    			this.maxPropiedades = 2;
    			break;
    		case MonopolyDeal.grupoCeleste:
    			this.nivel1 = 1;
    			this.nivel2 = 2;
    			this.nivel3 = 3;
    			this.maxPropiedades = 3;
    			break;
    		case MonopolyDeal.grupoRosa:
    			this.nivel1 = 1;
    			this.nivel2 = 2;
    			this.nivel3 = 4;
    			this.maxPropiedades = 3;
    			break;
    		case MonopolyDeal.grupoNaranja:
    			this.nivel1 = 1;
    			this.nivel2 = 3;
    			this.nivel3 = 5;
    			this.maxPropiedades = 3;
    			break;
    		case MonopolyDeal.grupoRojo:
    			this.nivel1 = 2;
    			this.nivel2 = 3;
    			this.nivel3 = 6;
    			this.maxPropiedades = 3;
    			break;
    		case MonopolyDeal.grupoAmarillo:
    			this.nivel1 = 2;
    			this.nivel2 = 4;
    			this.nivel3 = 6;
    			this.maxPropiedades = 3;
    			break;
    		case MonopolyDeal.grupoVerde:
    			this.nivel1 = 2;
    			this.nivel2 = 4;
    			this.nivel3 = 7;
    			this.maxPropiedades = 3;
    			break;
    		case MonopolyDeal.grupoAzul:
    			this.nivel1 = 3;
    			this.nivel2 = 8;
    			this.maxPropiedades = 2;
    			break;
    		case MonopolyDeal.grupoFerrocarril:
    			this.nivel1 = 1;
    			this.nivel2 = 2;
    			this.nivel3 = 3;
    			this.nivel4 = 4;
    			this.maxPropiedades = 4;
    			break;
    		case MonopolyDeal.grupoServicio:
    			this.nivel1 = 1;
    			this.nivel2 = 2;
    			this.maxPropiedades = 2;
    			break;
    	}
    	this.color = color;
    	this.propiedades = new ArrayList<Carta>();
    }
    
    public boolean addCarta (Carta c){
    	if (c.getTipo() == MonopolyDeal.cartaAccion){
    		if (c.getAccion() == MonopolyDeal.accionCasa){
    			return this.addCasa(c);
    		} else if (c.getAccion() == MonopolyDeal.accionHotel){
    			return this.addHotel(c);
    		}
    		return false;
    	} else if (!this.grupoLleno() && this.cartaValida(c)){
            this.propiedades.add(c);
            return true;
        }
        return false;
    }
    
    public boolean cartaValida (Carta c){
		if (c.getTipo() == MonopolyDeal.cartaPropiedad){
			if (c.getGrupo() == this.color){
				return true;
			}
		} else {
			if (c.getTipo() == MonopolyDeal.cartaComodin){
				if (c.checkGrupo(this.color)){
					return true;
				}
			}
		}
		return false;
	}
    
    public boolean grupoVacio (){
    	return this.propiedades.isEmpty();
    }
    
	public boolean grupoLleno (){
		return this.propiedades.size() == this.maxPropiedades;
	}
    
    public boolean addCasa (Carta c){
    	if (c.getAccion() == MonopolyDeal.accionCasa){
    		if (this.grupoLleno() && !this.tieneCasa()){
    			if ((this.color != MonopolyDeal.grupoServicio) && (this.color != MonopolyDeal.grupoFerrocarril)){
					this.casa = c;
					return true;
				}
    		}
    	}
    	return false;
    }
    
    public boolean addHotel (Carta c){
    	if (c.getAccion() == MonopolyDeal.accionHotel){
    		if (this.tieneCasa()){
    			if (!this.tieneHotel()){
    				if ((this.color != MonopolyDeal.grupoServicio) && (this.color != MonopolyDeal.grupoFerrocarril)){
    					this.hotel = c;
    					return true;
    				}
    			}
    		}
    	}
    	return false;
    }

	public boolean tieneCasa (){
		return this.casa != null;
	}
	
	public boolean tieneHotel (){
		return this.hotel != null;
	}

	public int getValor (){
		int valor;
		switch (this.getQPropiedades()){
			case 1:
				valor = this.nivel1;
				break;
			case 2:
				valor = this.nivel2;
				break;
			case 3:
				valor = this.nivel3;
				break;
			case 4:
				valor = this.nivel4;
				break;
			default:
				valor = 0;
				break;
		}
		if (this.tieneCasa()){
			valor = valor + 4;
		}
		if (this.tieneHotel()){
			valor = valor + 4;
		}
		return valor;
	}

	private int getQPropiedades() {
		return this.propiedades.size();
	}
	
	public String[] getList (){
		if (!this.grupoVacio()){
			ArrayList<String> cartas = new ArrayList<String>();
			for (int i = 0; i < this.propiedades.size(); i++){
				cartas.add(this.propiedades.get(i).toString());
			}
			if (this.tieneCasa()){
				cartas.add(this.casa.toString());
			}
			if (this.tieneHotel()){
				cartas.add(this.hotel.toString());
			}
			cartas.add("- Renta: " + this.getValor() + "M");
			return cartas.toArray(new String[0]);
		}
		return null;
	}
}
