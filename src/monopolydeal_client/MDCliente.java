package monopolydeal_client;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JToolBar;

import java.awt.BorderLayout;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;

import java.awt.FlowLayout;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.BoxLayout;
import javax.swing.JTextField;

import java.awt.GridLayout;

import javax.swing.JSplitPane;
import javax.swing.ImageIcon;

import java.awt.Toolkit;

import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.JSeparator;
import javax.swing.JList;
import javax.swing.border.TitledBorder;

import java.awt.SystemColor;

import javax.swing.SwingConstants;
import javax.swing.JButton;

import java.awt.Color;

import javax.swing.border.LineBorder;

import monopolydeal.*;
import monopolydeal_server.ServerCommands;

import javax.swing.ListSelectionModel;
import javax.swing.AbstractListModel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class MDCliente {
	private JFrame frmClienteDeMonopoly;
	
	private Cliente cliente;
	private Jugador jugador;
	
	private JTextField textJugador;
	private JTextField textJugadorActual;
	private JTextField textMovimientos;
	
	private JList listJugadores;
	private JList listGrupo0;
	private JList listGrupo1;
	private JList listGrupo2;
	private JList listGrupo3;
	private JList listGrupo4;
	private JList listGrupo5;
	private JList listGrupo6;
	private JList listGrupo7;
	private JList listGrupo8;
	private JList listGrupo9;
	private JList listMano;
	private JList listBanco;

	private String[] rentaVacio = {"Vacio", "- Renta: 0M"};
	private String[] bancoVacio = {"Vacio", "- Dinero: 0M"};
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MDCliente window = new MDCliente();
					window.frmClienteDeMonopoly.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MDCliente() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmClienteDeMonopoly = new JFrame();
		frmClienteDeMonopoly.getContentPane().setEnabled(false);
		frmClienteDeMonopoly.setIconImage(Toolkit.getDefaultToolkit().getImage(MDCliente.class.getResource("/monopolydeal_icons/controller.png")));
		frmClienteDeMonopoly.setResizable(false);
		frmClienteDeMonopoly.setTitle("Cliente de Monopoly Deal");
		frmClienteDeMonopoly.setBounds(100, 100, 881, 553);
		frmClienteDeMonopoly.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frmClienteDeMonopoly.setJMenuBar(menuBar);
		
		JMenu mnMen = new JMenu("Opciones");
		mnMen.setIcon(new ImageIcon(MDCliente.class.getResource("/monopolydeal_icons/cog.png")));
		menuBar.add(mnMen);
		
		JMenuItem mntmConectar = new JMenuItem("Conectar");
		mntmConectar.setIcon(new ImageIcon(MDCliente.class.getResource("/monopolydeal_icons/server_connect.png")));
		mntmConectar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String serverHost = (String) JOptionPane.showInputDialog(frmClienteDeMonopoly, "Ingrese la direcci�n del servidor:", "Conectar a Servidor", JOptionPane.QUESTION_MESSAGE, null, null, "127.0.0.1");
				int serverPort = Integer.parseInt((String) JOptionPane.showInputDialog(frmClienteDeMonopoly, "Ingrese el puerto del servidor:", "Conectar a Servidor", JOptionPane.QUESTION_MESSAGE, null, null, "5824"));
				String clientName = (String) JOptionPane.showInputDialog(frmClienteDeMonopoly, "Ingrese su nombre:", "Conectar a Servidor", JOptionPane.QUESTION_MESSAGE, null, null, "Natalia Natalia");
				cliente = new Cliente (clientName, serverHost, serverPort);
				if (cliente.connect()){
					JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Ya estas conectado al servidor.", "Conectar a Servidor", JOptionPane.INFORMATION_MESSAGE);
				} else {
					JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Error conectando al servidor.\n" + cliente.getError(), "Conectar a Servidor", JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		mnMen.add(mntmConectar);
		
		JMenuItem mntmAcercaDeMonopoly = new JMenuItem("Acerca de Monopoly Deal");
		mntmAcercaDeMonopoly.setIcon(new ImageIcon(MDCliente.class.getResource("/monopolydeal_icons/help.png")));
		mntmAcercaDeMonopoly.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Monopoly y Monopoly Deal son marcas de Hasbro.\nEscrito en Java.\nProgramado en Argentina.\n", "Acerca de Monopoly Deal", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		mnMen.add(mntmAcercaDeMonopoly);
		
		JMenuItem mntmSalir = new JMenuItem("Salir");
		mntmSalir.setIcon(new ImageIcon(MDCliente.class.getResource("/monopolydeal_icons/door_open.png")));
		mntmSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmClienteDeMonopoly.dispose();
			}
		});
		mnMen.add(mntmSalir);
		
		JMenu mnHola = new JMenu("Hola");
		menuBar.add(mnHola);
		
		JMenuItem mntmJugadorTest = new JMenuItem("Jugador Test");
		mntmJugadorTest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// A borrar
				jugador = new Jugador (1, "Facundo");
				Carta c;
				c = new CartaDinero(1);
				jugador.addDinero(c);
				c = new CartaPropiedad(MonopolyDeal.grupoAmarillo);
				jugador.addDinero(c);
				c = new CartaDinero(5);
				jugador.addDinero(c);
				c = new CartaDinero(5);
				jugador.addCarta(c);
				c = new CartaAccion(MonopolyDeal.accionTratoHecho);
				jugador.addDinero(c);
				c = new CartaPropiedad(MonopolyDeal.grupoCafe);
				jugador.addPropiedad(MonopolyDeal.grupoCafe, c);
				c = new CartaPropiedad(MonopolyDeal.grupoAzul);
				jugador.addPropiedad(MonopolyDeal.grupoAzul, c);
				c = new CartaPropiedad(MonopolyDeal.grupoAzul);
				jugador.addPropiedad(MonopolyDeal.grupoAzul, c);
				c = new CartaAccion(MonopolyDeal.accionCasa);
				jugador.addPropiedad(MonopolyDeal.grupoAzul, c);
				c = new CartaAccion(MonopolyDeal.accionCancelar);
				jugador.addCarta(c);
				c = new CartaPropiedad(MonopolyDeal.grupoCafe);
				jugador.addCarta(c);
				actualizarJugador();
				// A borrar
			}
		});
		mnHola.add(mntmJugadorTest);
		frmClienteDeMonopoly.getContentPane().setLayout(null);
		
		JLabel lblJugador = new JLabel("Jugador");
		lblJugador.setBounds(10, 11, 47, 14);
		frmClienteDeMonopoly.getContentPane().add(lblJugador);
		
		textJugador = new JTextField();
		textJugador.setText("Desconocido");
		textJugador.setEditable(false);
		textJugador.setBounds(67, 8, 200, 20);
		frmClienteDeMonopoly.getContentPane().add(textJugador);
		textJugador.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Turno Actual");
		lblNewLabel.setBounds(573, 11, 72, 14);
		frmClienteDeMonopoly.getContentPane().add(lblNewLabel);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 36, 848, 2);
		frmClienteDeMonopoly.getContentPane().add(separator);
		
		textJugadorActual = new JTextField();
		textJugadorActual.setText("Desconocido");
		textJugadorActual.setEditable(false);
		textJugadorActual.setBounds(663, 8, 195, 20);
		frmClienteDeMonopoly.getContentPane().add(textJugadorActual);
		textJugadorActual.setColumns(10);
		
		JButton btnUsar = new JButton("Usar");
		btnUsar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = listMano.getSelectedIndex();
				int d;
				Carta c = jugador.viewCarta(i);
				switch (c.getTipo()){
					case MonopolyDeal.cartaPropiedad:
						d = JOptionPane.showConfirmDialog(frmClienteDeMonopoly, "�Desea agregar la PROPIEDAD \"" + c.getNombre() + "\" al grupo correspondiente?", "Agregar Propiedad", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
						if (d == JOptionPane.YES_OPTION){
							String cmd = ServerCommands.addPropiedad(i);
							JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Comando: " + cmd, null, JOptionPane.INFORMATION_MESSAGE);
						}
						break;
						
					case MonopolyDeal.cartaDinero:
						JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Las cartas de tipo DINERO solo se pueden guardar en el banco.", "Ayuda", JOptionPane.INFORMATION_MESSAGE);
						break;
						
					case MonopolyDeal.cartaAccion:
						switch (c.getAccion()){
							case MonopolyDeal.accionCancelar:
								JOptionPane.showMessageDialog(frmClienteDeMonopoly, "La ACCI�N \"" + c.getNombre() + "\" no se puede usar durante tu propio turno.", "Ayuda", JOptionPane.INFORMATION_MESSAGE);
								break;
							
							case MonopolyDeal.accionCasa:
							case MonopolyDeal.accionHotel:
								d = JOptionPane.showConfirmDialog(frmClienteDeMonopoly, "�Desea usar la ACCI�N \"" + c.getNombre() + "\"?", "Usar Acci�n", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
								if (d == JOptionPane.YES_OPTION){
									String cmd = ServerCommands.useAccion(i);
									JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Comando: " + cmd, null, JOptionPane.INFORMATION_MESSAGE);
								}
								break;
						}
						break;
				}
			}
		});
		btnUsar.setToolTipText("Realiza una acci\u00F3n dependiendo del tipo de carta.");
		btnUsar.setBounds(658, 313, 205, 26);
		frmClienteDeMonopoly.getContentPane().add(btnUsar);
		
		JButton btnDescartar = new JButton("Descartar");
		btnDescartar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = listMano.getSelectedIndex();
				String cmd = ServerCommands.dumpCarta(i);
				JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Comando: " + cmd, null, JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnDescartar.setToolTipText("Envia la carta seleccionada al mazo de descarte.");
		btnDescartar.setBounds(658, 389, 205, 26);
		frmClienteDeMonopoly.getContentPane().add(btnDescartar);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int i = listMano.getSelectedIndex();
				String cmd = ServerCommands.saveCarta(i);
				JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Comando: " + cmd, null, JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnGuardar.setToolTipText("Guarda la carta seleccionada en el banco.");
		btnGuardar.setBounds(658, 351, 205, 26);
		frmClienteDeMonopoly.getContentPane().add(btnGuardar);
		
		JButton btnTerminar = new JButton("Terminar");
		btnTerminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String cmd = ServerCommands.endTurno();
				JOptionPane.showMessageDialog(frmClienteDeMonopoly, "Comando: " + cmd, null, JOptionPane.INFORMATION_MESSAGE);
			}
		});
		btnTerminar.setToolTipText("Termina el turno actual.");
		btnTerminar.setBounds(658, 427, 205, 26);
		frmClienteDeMonopoly.getContentPane().add(btnTerminar);
		
		textMovimientos = new JTextField();
		textMovimientos.setHorizontalAlignment(SwingConstants.TRAILING);
		textMovimientos.setText("0");
		textMovimientos.setEditable(false);
		textMovimientos.setBounds(805, 465, 58, 20);
		frmClienteDeMonopoly.getContentPane().add(textMovimientos);
		textMovimientos.setColumns(10);
		
		JLabel lblMovimientosRestantes = new JLabel("Movimientos Restantes");
		lblMovimientosRestantes.setBounds(658, 467, 133, 16);
		frmClienteDeMonopoly.getContentPane().add(lblMovimientosRestantes);
		
		JSeparator separator_3 = new JSeparator();
		separator_3.setOrientation(SwingConstants.VERTICAL);
		separator_3.setBounds(644, 37, 2, 461);
		frmClienteDeMonopoly.getContentPane().add(separator_3);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(646, 161, 214, 2);
		frmClienteDeMonopoly.getContentPane().add(separator_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setViewportBorder(new TitledBorder(null, "Grupo Cafe", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(139, 69, 19)));
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(10, 50, 200, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane);
		
		listGrupo0 = new JList();
		listGrupo0.setEnabled(false);
		scrollPane.setViewportView(listGrupo0);
		listGrupo0.setValueIsAdjusting(true);
		listGrupo0.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo0.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo0.setBackground(SystemColor.control);
		listGrupo0.setBorder(null);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setViewportBorder(new TitledBorder(null, "Grupo Celeste", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(30, 144, 255)));
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_1.setBounds(222, 50, 200, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_1);
		
		listGrupo1 = new JList();
		listGrupo1.setEnabled(false);
		scrollPane_1.setViewportView(listGrupo1);
		listGrupo1.setValueIsAdjusting(true);
		listGrupo1.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo1.setBorder(null);
		listGrupo1.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setViewportBorder(new TitledBorder(null, "Grupo Rosa", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 105, 180)));
		scrollPane_2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_2.setBounds(434, 50, 195, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_2);
		
		listGrupo2 = new JList();
		listGrupo2.setEnabled(false);
		scrollPane_2.setViewportView(listGrupo2);
		listGrupo2.setValueIsAdjusting(true);
		listGrupo2.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo2.setBorder(null);
		listGrupo2.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setViewportBorder(new TitledBorder(null, "Grupo Naranja", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 140, 0)));
		scrollPane_3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_3.setBounds(10, 161, 200, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_3);
		
		listGrupo3 = new JList();
		listGrupo3.setEnabled(false);
		scrollPane_3.setViewportView(listGrupo3);
		listGrupo3.setValueIsAdjusting(true);
		listGrupo3.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo3.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo3.setBorder(null);
		listGrupo3.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setViewportBorder(new TitledBorder(null, "Grupo Rojo", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 0, 0)));
		scrollPane_4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_4.setBounds(222, 161, 200, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_4);
		
		listGrupo4 = new JList();
		listGrupo4.setEnabled(false);
		scrollPane_4.setViewportView(listGrupo4);
		listGrupo4.setValueIsAdjusting(true);
		listGrupo4.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo4.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo4.setBorder(null);
		listGrupo4.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5.setViewportBorder(new TitledBorder(null, "Grupo Amarillo", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(255, 215, 0)));
		scrollPane_5.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_5.setBounds(434, 161, 195, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_5);
		
		listGrupo5 = new JList();
		listGrupo5.setEnabled(false);
		scrollPane_5.setViewportView(listGrupo5);
		listGrupo5.setValueIsAdjusting(true);
		listGrupo5.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo5.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo5.setBorder(null);
		listGrupo5.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_6 = new JScrollPane();
		scrollPane_6.setViewportBorder(new TitledBorder(null, "Grupo Verde", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 100, 0)));
		scrollPane_6.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_6.setBounds(10, 272, 200, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_6);
		
		listGrupo6 = new JList();
		listGrupo6.setEnabled(false);
		scrollPane_6.setViewportView(listGrupo6);
		listGrupo6.setValueIsAdjusting(true);
		listGrupo6.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo6.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo6.setBorder(null);
		listGrupo6.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_7 = new JScrollPane();
		scrollPane_7.setViewportBorder(new TitledBorder(null, "Grupo Azul", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 255)));
		scrollPane_7.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_7.setBounds(222, 272, 200, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_7);
		
		listGrupo7 = new JList();
		listGrupo7.setEnabled(false);
		scrollPane_7.setViewportView(listGrupo7);
		listGrupo7.setValueIsAdjusting(true);
		listGrupo7.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo7.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo7.setBorder(null);
		listGrupo7.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_8 = new JScrollPane();
		scrollPane_8.setViewportBorder(new TitledBorder(null, "Grupo Ferrocarril", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		scrollPane_8.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_8.setBounds(434, 272, 200, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_8);
		
		listGrupo8 = new JList();
		listGrupo8.setEnabled(false);
		scrollPane_8.setViewportView(listGrupo8);
		listGrupo8.setValueIsAdjusting(true);
		listGrupo8.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo8.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo8.setBorder(null);
		listGrupo8.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_9 = new JScrollPane();
		scrollPane_9.setViewportBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Grupo Servicio", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(154, 205, 50)));
		scrollPane_9.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_9.setBounds(10, 386, 200, 104);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_9);
		
		listGrupo9 = new JList();
		listGrupo9.setEnabled(false);
		scrollPane_9.setViewportView(listGrupo9);
		listGrupo9.setValueIsAdjusting(true);
		listGrupo9.setModel(new AbstractListModel() {
			String[] values = rentaVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listGrupo9.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listGrupo9.setBorder(null);
		listGrupo9.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_10 = new JScrollPane();
		scrollPane_10.setViewportBorder(new TitledBorder(null, "Banco", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(153, 50, 204)));
		scrollPane_10.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_10.setBounds(434, 389, 200, 101);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_10);
		
		listBanco = new JList();
		listBanco.setEnabled(false);
		scrollPane_10.setViewportView(listBanco);
		listBanco.setValueIsAdjusting(true);
		listBanco.setModel(new AbstractListModel() {
			String[] values = bancoVacio;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listBanco.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listBanco.setBorder(null);
		listBanco.setBackground(SystemColor.control);
		
		JScrollPane scrollPane_11 = new JScrollPane();
		scrollPane_11.setViewportBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Jugadores", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		scrollPane_11.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_11.setBounds(658, 50, 205, 99);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_11);
		
		listJugadores = new JList();
		scrollPane_11.setViewportView(listJugadores);
		listJugadores.setValueIsAdjusting(true);
		listJugadores.setModel(new AbstractListModel() {
			String[] values = new String[] {"Vacio"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listJugadores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listJugadores.setBackground(SystemColor.control);
		listJugadores.setBorder(null);
		
		JScrollPane scrollPane_12 = new JScrollPane();
		scrollPane_12.setViewportBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Mano", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		scrollPane_12.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane_12.setBounds(658, 175, 205, 126);
		frmClienteDeMonopoly.getContentPane().add(scrollPane_12);
		
		listMano = new JList();
		scrollPane_12.setViewportView(listMano);
		listMano.setValueIsAdjusting(true);
		listMano.setModel(new AbstractListModel() {
			String[] values = new String[] {"Vacio"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		listMano.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listMano.setBackground(SystemColor.control);
		listMano.setBorder(null);
	}
	
	private void actualizarExtras (String[] js, int jActual){
		textJugadorActual.setText(js[jActual]);
		listJugadores.setListData(js);
	}
	
	private void actualizarJugador (){
		textJugador.setText(jugador.toString());
		for (int i = 0; i < MonopolyDeal.grupos; i++){
			Grupo g = jugador.getGrupo(i);
			actualizarGrupo (i, g.getList());
		}
		String[] list = jugador.getBanco().getList();
		listBanco.setEnabled(list != null);
		listBanco.setListData(checkList(list));
		listMano.setListData(checkList(jugador.getList()));
	}
	
	private void actualizarGrupo (int i, String[] list){
		switch (i){
			case MonopolyDeal.grupoCafe:
				listGrupo0.setEnabled(list != null);
				listGrupo0.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoCeleste:
				listGrupo1.setEnabled(list != null);
				listGrupo1.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoRosa:
				listGrupo2.setEnabled(list != null);
				listGrupo2.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoNaranja:
				listGrupo3.setEnabled(list != null);
				listGrupo3.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoRojo:
				listGrupo4.setEnabled(list != null);
				listGrupo4.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoAmarillo:
				listGrupo5.setEnabled(list != null);
				listGrupo5.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoVerde:
				listGrupo6.setEnabled(list != null);
				listGrupo6.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoAzul:
				listGrupo7.setEnabled(list != null);
				listGrupo7.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoFerrocarril:
				listGrupo8.setEnabled(list != null);
				listGrupo8.setListData(checkList(list));
				break;
			case MonopolyDeal.grupoServicio:
				listGrupo9.setEnabled(list != null);
				listGrupo9.setListData(checkList(list));
				break;
		}
	}
	
	private String[] checkList (String[] list){
		if (list == null){
			list = rentaVacio;
		}
		return list;
	}
}
