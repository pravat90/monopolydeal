package monopolydeal; 

final public class CartaPropiedad extends Carta {
	private static final long serialVersionUID = 4590264993842183765L;

	private int grupo;
    
    private static int qCafe = 0;
    private static int qCeleste = 0;
    private static int qRosa = 0;
    private static int qNaranja = 0;
    private static int qRojo = 0;
    private static int qAmarillo = 0;
    private static int qVerde = 0;
    private static int qAzul = 0;
    private static int qFerrocarril = 0;
    private static int qServicio = 0;
    
    public CartaPropiedad (int grupo){
        this.nombre = CartaPropiedad.setNombre(grupo);
        this.tipo = MonopolyDeal.cartaPropiedad;
        switch (grupo){
            case MonopolyDeal.grupoCafe:
            case MonopolyDeal.grupoCeleste:
                this.valor = 1;
                break;
            case MonopolyDeal.grupoRosa:
            case MonopolyDeal.grupoNaranja:
            case MonopolyDeal.grupoFerrocarril:
            case MonopolyDeal.grupoServicio:
                this.valor = 2;
                break;
            case MonopolyDeal.grupoRojo:
            case MonopolyDeal.grupoAmarillo:
                this.valor = 3;
                break;
            case MonopolyDeal.grupoVerde:
            case MonopolyDeal.grupoAzul:
                this.valor = 4;
                break;
        }
        this.grupo = grupo;
    }
    
    @Override
    public int getGrupo (){
        return this.grupo;
    }
    
    @Override
    public String toString(){
		return this.nombre + " (" + this.valor + "M)";
    }
    
    private static String setNombre (int grupo){
    	int p;
    	switch (grupo){
    		case MonopolyDeal.grupoCafe:
    			p = CartaPropiedad.qCafe;
    			if (CartaPropiedad.qCafe < MonopolyDeal.nombreCafe.length){
    				CartaPropiedad.qCafe++;
    			} else {
    				CartaPropiedad.qCafe = 0;
    			};
    			return MonopolyDeal.nombreCafe[p];
    		case MonopolyDeal.grupoCeleste:
    			p = CartaPropiedad.qCeleste;
    			if (CartaPropiedad.qCeleste < MonopolyDeal.nombreCeleste.length){
    				CartaPropiedad.qCeleste++;
    			} else {
    				CartaPropiedad.qCeleste = 0;
    			};
    			return MonopolyDeal.nombreCeleste[p];
    		case MonopolyDeal.grupoRosa:
    			p = CartaPropiedad.qRosa;
    			if (CartaPropiedad.qRosa < MonopolyDeal.nombreRosa.length){
    				CartaPropiedad.qRosa++;
    			} else {
    				CartaPropiedad.qRosa = 0;
    			};
    			return MonopolyDeal.nombreRosa[p];
    		case MonopolyDeal.grupoNaranja:
    			p = CartaPropiedad.qNaranja;
    			if (CartaPropiedad.qNaranja < MonopolyDeal.nombreNaranja.length){
    				CartaPropiedad.qNaranja++;
    			} else {
    				CartaPropiedad.qNaranja = 0;
    			};
    			return MonopolyDeal.nombreNaranja[p];
    		case MonopolyDeal.grupoRojo:
    			p = CartaPropiedad.qRojo;
    			if (CartaPropiedad.qRojo < MonopolyDeal.nombreRojo.length){
    				CartaPropiedad.qRojo++;
    			} else {
    				CartaPropiedad.qRojo = 0;
    			};
    			return MonopolyDeal.nombreRojo[p];
    		case MonopolyDeal.grupoAmarillo:
    			p = CartaPropiedad.qAmarillo;
    			if (CartaPropiedad.qAmarillo < MonopolyDeal.nombreAmarillo.length){
    				CartaPropiedad.qAmarillo++;
    			} else {
    				CartaPropiedad.qAmarillo = 0;
    			};
    			return MonopolyDeal.nombreAmarillo[p];
    		case MonopolyDeal.grupoVerde:
    			p = CartaPropiedad.qVerde;
    			if (CartaPropiedad.qVerde < MonopolyDeal.nombreVerde.length){
    				CartaPropiedad.qVerde++;
    			} else {
    				CartaPropiedad.qVerde = 0;
    			};
    			return MonopolyDeal.nombreVerde[p];
    		case MonopolyDeal.grupoAzul:
    			p = CartaPropiedad.qAzul;
    			if (CartaPropiedad.qAzul < MonopolyDeal.nombreAzul.length){
    				CartaPropiedad.qAzul++;
    			} else {
    				CartaPropiedad.qAzul = 0;
    			};
    			return MonopolyDeal.nombreAzul[p];
    		case MonopolyDeal.grupoFerrocarril:
    			p = CartaPropiedad.qFerrocarril;
    			if (CartaPropiedad.qFerrocarril < MonopolyDeal.nombreFerrocarril.length){
    				CartaPropiedad.qFerrocarril++;
    			} else {
    				CartaPropiedad.qFerrocarril = 0;
    			};
    			return MonopolyDeal.nombreFerrocarril[p];
    		case MonopolyDeal.grupoServicio:
    			p = CartaPropiedad.qServicio;
    			if (CartaPropiedad.qServicio < MonopolyDeal.nombreServicio.length){
    				CartaPropiedad.qServicio++;
    			} else { 
    				CartaPropiedad.qServicio = 0;
    			};
    			return MonopolyDeal.nombreServicio[p];
    		default:
    			return "";
    	}
    }
    
    public static void reset (){
    	CartaPropiedad.qCafe = 0;
    	CartaPropiedad.qCeleste = 0;
    	CartaPropiedad.qRosa = 0;
    	CartaPropiedad.qNaranja = 0;
    	CartaPropiedad.qAmarillo = 0;
    	CartaPropiedad.qRojo = 0;
    	CartaPropiedad.qVerde = 0;
    	CartaPropiedad.qAzul = 0;
    	CartaPropiedad.qFerrocarril = 0;
    	CartaPropiedad.qServicio = 0;
    }
}
