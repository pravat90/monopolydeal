package monopolydeal; 

public class CartaRenta extends Carta {
	private static final long serialVersionUID = 1347334748911191607L;
	
	private int grupo;
    
    public CartaRenta (int grupo){
        switch (grupo){
            case MonopolyDeal.rentaCelesteCafe:
                this.nombre = "Celeste/Cafe";
                this.valor = 1;
                break;
            case MonopolyDeal.rentaRosaNaranja:
                this.nombre = "Rosa/Naranja";
                this.valor = 1;
                break;
            case MonopolyDeal.rentaRojoAmarillo:
                this.nombre = "Rojo/Amarillo";
                this.valor = 1;
                break;
            case MonopolyDeal.rentaAzulVerde:
                this.nombre = "Azul/Verde";
                this.valor = 1;
                break;
            case MonopolyDeal.rentaFerrocarrilServicio:
                this.nombre = "Ferrocarril/Servicio";
                this.valor = 1;
                break;
            case MonopolyDeal.rentaMulti:
                this.nombre = "Multicolor";
                this.valor = 3;
                break;
        }
        this.tipo = MonopolyDeal.cartaRenta;
        this.grupo = grupo;
    }
    
    @Override
    public int getGrupo (){
        return this.grupo;
    }
    
    @Override
    public String toString (){
    	return "Renta " + this.nombre + " (" + this.valor + "M)";
    }
}
