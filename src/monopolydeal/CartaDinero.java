package monopolydeal; 

final public class CartaDinero extends Carta{
	private static final long serialVersionUID = -5818274025735184009L;

	public CartaDinero (int valor){
        this.nombre = valor + "M";
        this.tipo = MonopolyDeal.cartaDinero;
        this.valor = valor;
    }
    
    @Override
    public String toString (){
    	return "Billete (" + this.nombre +")";
    }
}
