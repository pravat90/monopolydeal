package monopolydeal;

public abstract class MonopolyDealRules {
	// Tipos de Cartas
	final public static int cartaDinero = 0;
    final public static int cartaComodin = 1;
    final public static int cartaPropiedad = 2;
    final public static int cartaAccion = 3;
    final public static int cartaRenta = 4;
	
	// Tipos de Grupos
	final public static int grupos = 10;
    final public static int grupoCafe = 0;
    final public static int grupoCeleste = 1;
    final public static int grupoRosa = 2;
    final public static int grupoNaranja = 3;
    final public static int grupoRojo = 4;
    final public static int grupoAmarillo = 5;
    final public static int grupoVerde = 6;
    final public static int grupoAzul = 7;
    final public static int grupoFerrocarril = 8;
    final public static int grupoServicio = 9;

    
    // Nombres Propiedades
    final public static String[] nombreCafe = {"Avenida Mediterr�neo", "Avenida B�ltica"};
    final public static String[] nombreCeleste = {"Avenida Vermont", "Avenida Connecticut", "Avenida Oriental"};
    final public static String[] nombreRosa = {"Avenida Virginia", "Plaza San Carlos", "Avenida Status"};
    final public static String[] nombreNaranja = {"Avenida Tennessee", "Plaza St. James", "Avenida Nueva York"};
    final public static String[] nombreRojo = {"Avenida Indiana", "Avenida Kentucky", "Avenida Illinois"};
    final public static String[] nombreAmarillo = {"Avenida Atl�ntico", "Jardines Marvin", "Avenida Ventnor"};
    final public static String[] nombreVerde = {"Avenida Carolina del Norte", "Avenida Pac�fico", "Avenida Pennsylvania"};
    final public static String[] nombreAzul = {"Plaza Park", "Paseo Maritimo"};
    final public static String[] nombreFerrocarril = {"Ferrocarril B. & O.", "V�a R�pida", "Ferrocarril Pennsylvania", "Ferrocarril Reading"};
    final public static String[] nombreServicio = {"Compa��a de Electricidad", "Compa��a de Agua"};
    
    // Tipos de Acciones
    final public static int acciones = 10;
    final public static int accionCumpleanios = 0;
    final public static int accionCasa = 1;
    final public static int accionHotel = 2;
    final public static int accionCancelar = 3;
    final public static int accionCobroDeuda = 4;
    final public static int accionDobleRenta = 5;
    final public static int accionAccesoLibre = 6;
    final public static int accionTratoForzoso = 7;
    final public static int accionTratoVencido = 8;
    final public static int accionTratoHecho = 9;
    
    // Tipos de Comodines
    final public static int comodines = 8;
    final public static int comodinCelesteCafe = 0;
    final public static int comodinCelesteFerrocarril = 1;
    final public static int comodinRosaNaranja = 2;
    final public static int comodinRojoAmarillo = 3;
    final public static int comodinAzulVerde = 4;
    final public static int comodinVerdeFerrocarril = 5;
    final public static int comodinFerrocarrilServicio = 6;
    final public static int comodinMulti = 7;
	
    // Tipos de Renta
    final public static int rentas = 6;
    final public static int rentaCelesteCafe = 0;
    final public static int rentaRosaNaranja = 1;
    final public static int rentaRojoAmarillo = 2;
    final public static int rentaAzulVerde = 3;
    final public static int rentaFerrocarrilServicio = 4;
    final public static int rentaMulti = 5;
    
    // Tipos de Estado
    final public static int estadoEspera = 0;
    final public static int estadoJugando = 1;
    
    // Cantidad Jugadores
    final public static int minJugadores = 2;
    final public static int maxJugadores = 4;
    
    final public static int gruposNecesarios = 3;
}
