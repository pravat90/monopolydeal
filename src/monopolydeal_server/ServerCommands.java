package monopolydeal_server;

public final class ServerCommands {
	public final static String cmdAddJugador = "add_jugador"; // add_jugador [s.Jugador]
	public final static String cmdGetJugador = "get_jugador"; // get_jugador [i.Jugador]
	public final static String cmdAddPropiedad = "add_propiedad"; // add_propiedad [i.Mano]
	public final static String cmdAddComodin = "add_comodin"; // add_comodin [i.Mano] [i.Grupo]
	public final static String cmdUseAccion = "use_accion"; // use_action [i.Mano] [i.Jugador]
	public final static String cmdUseRenta = "use_renta"; // use_renta [i.Mano] [i.Grupo]
	public final static String cmdDumpCarta = "dump_carta"; // dump_carta [i.Mano]
	public final static String cmdSaveCarta = "save_carta"; // save_carta [i.Mano]
	public final static String cmdEndTurno = "end_turno"; // end_turno

	public static String addJugador (String nombre){
		return cmdAddJugador + " " + nombre;
	}
	
	public static String addPropiedad (int indiceMano){
		if (checkValue(indiceMano)){
			return cmdAddPropiedad + " " + indiceMano;
		}
		return null;
	}
	
	public static String addComodin (int indicePropiedad, int indiceGrupo){
		if (checkValue(indicePropiedad) & checkValue(indiceGrupo)){
			return cmdAddComodin + " " + indicePropiedad + " " + indiceGrupo;
		}
		return null;
	}
	
	public static String useAccion (int indiceMano, int indiceJugador){
		if (checkValue(indiceMano)){
			return cmdUseAccion + " " + indiceMano + " " + indiceJugador;
		}
		return null;
	}
	public static String useAccion (int indiceMano){
		return useAccion(indiceMano, -1);
	}
	
	public static String useRenta (int indiceMano, int indiceGrupo, int indiceJugador){
		if (checkValue(indiceMano) & checkValue(indiceGrupo)){
			return cmdUseRenta + " " + indiceMano + " " + indiceGrupo + " " + indiceJugador;
		}
		return null;
	}
	public static String useRenta (int indiceMano, int indiceGrupo){
		return useRenta(indiceMano, indiceGrupo, -1);
	}
	
	public static String dumpCarta (int indiceMano){
		if (checkValue(indiceMano)){
			return cmdDumpCarta + " " + indiceMano;
		}
		return null;
	}
	
	public static String saveCarta (int indiceMano){
		if (checkValue(indiceMano)){
			return cmdSaveCarta + " " + indiceMano;
		}
		return null;
	}
	
	public static String endTurno (){
		return cmdEndTurno;
	}
	
	private static boolean checkValue (int i){
		return i > -1;
	}
	
	public static String[] splitCommand (String command){
		String[] cmd = command.split(" ");
		switch (cmd[0]){
			case ServerCommands.cmdAddPropiedad:
			case ServerCommands.cmdAddComodin:
			case ServerCommands.cmdUseAccion:
			case ServerCommands.cmdUseRenta:
			case ServerCommands.cmdDumpCarta:
			case ServerCommands.cmdEndTurno:
				return cmd;
			default:
				return null;
		}
		
	} 
}
