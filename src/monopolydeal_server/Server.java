package monopolydeal_server;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import monopolydeal.*;

public final class Server {
	private static Server serverConsole = new Server();
	private ServerSocket serverSocket;
	private Socket clientSocket;
	
	private Exception lastError;
	private boolean serverStatus = false;
	
	private int serverPort;
	private int maxPlayers;
	
	private MonopolyDeal monopolyDeal;
	
	private ArrayList<ClientThread> players;
	
	private Server (){
		this.monopolyDeal = MonopolyDeal.getInstance();
	}
	
	public Exception getError (){
		return this.lastError;
	}
	
	public boolean isRunning (){
		return this.serverStatus;
	}
	
	public static Server getInstance (){
		return Server.serverConsole;
	}
	
	public void setPort (int port){
		if (!this.isRunning()){
			this.serverPort = port;
		}
	}
	
	public void setPlayers (int players){
		if (!this.isRunning()){
			this.maxPlayers = players;
		}
	}

	public boolean connect (){
		try {
			this.serverSocket = new ServerSocket (this.serverPort);
			this.serverStatus = true;
			return true;
		} catch (Exception e){
			this.lastError = e;
			return false;
		}
	}
	
	public boolean getPlayer (){
		try {
			this.players.add(new ClientThread(this.serverSocket.accept()));
			return true;
		} catch (Exception e){
			this.lastError = e;
			return false;
		}
	}
	
	public boolean disconnect (){
		try {
			this.serverSocket.close();
			this.serverStatus = false;
			return true;
		} catch (Exception e){
			this.lastError = e;
			return false;
		}
	}
}
