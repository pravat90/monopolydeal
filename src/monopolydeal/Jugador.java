package monopolydeal;

import java.io.Serializable;
import java.util.ArrayList;

public class Jugador implements Serializable {
	private static final long serialVersionUID = 7171213774729809323L;

	final private int maxMano = 7;
	
	private int id;
	private String nombre;
	private ArrayList<Carta> mano;
	private Grupo[] grupos;
	private Banco banco;
	
	public Jugador (int id, String nombre){
		this.id = id;
		this.nombre = nombre;
		this.mano = new ArrayList<Carta>();
		this.limpiarGrupos();
		this.banco = new Banco();
	}
	
	public int getId() {
		return id;
	}

	public String getNombre() {
		return nombre;
	}

	public boolean addCarta (Carta c){
		if (!this.manoLlena()){
			this.mano.add(c);
			return true;
		}
		return false;
	}
	
	public boolean addPropiedad (int n, Carta c){
		return this.grupos[n].addCarta(c);
	}
	
	public boolean addDinero (Carta c){
		return this.banco.addCarta(c);
	}
	
	public boolean manoVacia (){
		return this.mano.isEmpty();
	}
	
	public boolean manoLlena (){
		if (this.mano.size() == this.maxMano){
			return true;
		}
		return false;
	}
	
	public void limpiarGrupos (){
		this.grupos = new Grupo[MonopolyDeal.grupos];
		this.grupos[0] = new Grupo(MonopolyDeal.grupoCafe);
		this.grupos[1] = new Grupo(MonopolyDeal.grupoCeleste);
		this.grupos[2] = new Grupo(MonopolyDeal.grupoRosa);
		this.grupos[3] = new Grupo(MonopolyDeal.grupoNaranja);
		this.grupos[4] = new Grupo(MonopolyDeal.grupoRojo);
		this.grupos[5] = new Grupo(MonopolyDeal.grupoAmarillo);
		this.grupos[6] = new Grupo(MonopolyDeal.grupoVerde);
		this.grupos[7] = new Grupo(MonopolyDeal.grupoAzul);
		this.grupos[8] = new Grupo(MonopolyDeal.grupoFerrocarril);
		this.grupos[9] = new Grupo(MonopolyDeal.grupoServicio);
	}

	@Override
	public String toString() {
		return this.nombre + " (" + this.qGruposCompletos() +"/" + this.qGruposAdquiridos() + ")(" + this.banco.getValor() + "M)";
	}

	public Carta getCarta(int n) {
		if (!this.manoVacia()){
			return this.mano.remove(n);
		}
		return null;
	}
	
	public Carta viewCarta(int n) {
		if (!this.manoVacia()){
			return this.mano.get(n);
		}
		return null;
	}
	
	public int qGruposCompletos (){
		int q = 0;
		for (int i = 0; i < MonopolyDeal.grupos; i++){
			if (this.grupos[i].grupoLleno()){
				q ++;
			}
		}
		return q;
	}
	
	public int qGruposAdquiridos (){
		int q = 0;
		for (int i = 0; i < MonopolyDeal.grupos; i++){
			if (!this.grupos[i].grupoVacio()){
				q ++;
			}
		}
		return q;
	}
	
	public boolean gruposNecesarios (){
		return this.qGruposCompletos() == MonopolyDeal.gruposNecesarios;
	}

	public Grupo getGrupo(int i) {
		if ((i > -1) && (i < MonopolyDeal.grupos)){
			return this.grupos[i];
		}
		return null;
	}

	public Banco getBanco() {
		return this.banco;
	}

	public String[] getList() {
		if (!this.manoVacia()){
			String[] cartas = new String[this.mano.size()];
			for (int i = 0; i < this.mano.size(); i++){
				cartas[i] = this.mano.get(i).toString();
			}
			return cartas;
		} else {
			return null;
		}
	}
}
