package monopolydeal; 

final public class CartaAccion extends Carta{
	private static final long serialVersionUID = 4127213021582331800L;
	
	private int accion;
    
    public CartaAccion (int accion){
        switch (accion){
            case MonopolyDeal.accionCumpleanios:
                this.nombre = "Feliz Cumplea�os";
                this.valor = 2;
                break;
            case MonopolyDeal.accionCasa:
                this.nombre = "Casa";
                this.valor = 3;
                break;
            case MonopolyDeal.accionHotel:
                this.nombre = "Hotel";
                this.valor = 4;
                break;
            case MonopolyDeal.accionCancelar:
                this.nombre = "�Cancelado!";
                this.valor = 4;
                break;
            case MonopolyDeal.accionCobroDeuda:
                this.nombre = "Cobra Deudas";
                this.valor = 3;
                break;
            case MonopolyDeal.accionDobleRenta:
                this.nombre = "Doble Renta";
                this.valor = 1;
                break;
            case MonopolyDeal.accionAccesoLibre:
                this.nombre = "Acceso Libre";
                this.valor = 1;
                break;
            case MonopolyDeal.accionTratoForzoso:
                this.nombre = "Trato Forzoso";
                this.valor = 3;
                break;
            case MonopolyDeal.accionTratoVencido:
                this.nombre = "Trato Vencido";
                this.valor = 5;
                break;
            case MonopolyDeal.accionTratoHecho:
                this.nombre = "Trato Hecho";
                this.valor = 3;
                break;
        }
        this.tipo = MonopolyDeal.cartaAccion;
        this.accion = accion;
    }
    
    @Override
    public int getAccion (){
        return this.accion;
    }
    
    @Override
    public String toString (){
		return this.nombre + " (" + this.valor + "M)";
	}
}
