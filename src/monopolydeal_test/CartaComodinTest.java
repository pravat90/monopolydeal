package monopolydeal_test;

import monopolydeal.*;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CartaComodinTest {
	CartaComodin c1, c2, c3, c4;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		c1 = new CartaComodin(MonopolyDeal.comodinMulti);
		c2 = new CartaComodin(MonopolyDeal.comodinCelesteCafe);
		c3 = new CartaComodin(MonopolyDeal.comodinCelesteFerrocarril);
		c4 = new CartaComodin(MonopolyDeal.comodinFerrocarrilServicio);
	}

	@Test
	public void testCreate() {
		assertEquals(true, c1.checkGrupo(MonopolyDeal.grupoNaranja));
		assertEquals(false, c2.checkGrupo(MonopolyDeal.grupoNaranja));
		assertEquals(true, c3.checkGrupo(MonopolyDeal.grupoFerrocarril));
		assertEquals(true, c4.checkGrupo(MonopolyDeal.grupoFerrocarril));
	}

}
