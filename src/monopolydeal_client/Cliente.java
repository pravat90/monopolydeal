package monopolydeal_client;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import monopolydeal.Jugador;
import monopolydeal_server.ServerCommands;

public final class Cliente {
	private final String serverHost;
	private final int serverPort;
	
	private Socket clientSocket;
	private int clientID;
	private String clientName;
	private Exception lastError = null;
	
	private ObjectInputStream serverIn;
	private ObjectOutputStream clientOut;
	
	public Cliente (String nombre, String host, int port){
		this.clientName = nombre;
		this.serverHost = host;
		this.serverPort = port;
	}
	
	public boolean connect (){
		try {
			this.clientSocket = new Socket(this.serverHost, this.serverPort);
			this.serverIn = new ObjectInputStream(this.clientSocket.getInputStream());
			this.clientOut = new ObjectOutputStream(this.clientSocket.getOutputStream());
			this.clientOut.writeUTF(ServerCommands.addJugador(this.clientName));
			clientID = this.serverIn.read();
			return true;
		} catch (Exception e){
			this.lastError = e;
			return false;
		}
	}
	
	public boolean sendCommand (String command){
		try {
			this.clientOut.writeUTF(command);
			return true;
		} catch (Exception e){
			this.lastError = e;
			return false;
		}
	}
	
	public boolean getResponse (){
		try {
			return this.serverIn.readBoolean();
		} catch (Exception e){
			this.lastError = e;
			return false;
		}
	}
	
	public Jugador getData (){
		try {
			return (Jugador) this.serverIn.readObject();
		} catch (Exception e){
			this.lastError = e;
			return null;
		}
	}
	
	public String[] getArray (){
		try {
			return (String[]) this.serverIn.readObject();
		} catch (Exception e){
			this.lastError = e;
			return null;
		}
	}
	
	public boolean disconnect (){
		try {
			this.serverIn.close();
			this.clientOut.close();
			this.clientSocket.close();
			return true;
		} catch (Exception e){
			this.lastError = e;
			return false;
		}
	}
	
	public int getID (){
		return this.clientID;
	}
	
	public String getName (){
		return this.clientName;
	}
	
	public Exception getError (){
		return this.lastError;
	}

}
