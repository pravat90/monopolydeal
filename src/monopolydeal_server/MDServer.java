package monopolydeal_server;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SpinnerNumberModel;
import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.JTextArea;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.AbstractListModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JToggleButton;

import java.awt.Toolkit;
import java.util.ArrayList;

public class MDServer {
	private static Server server;
	private static int serverPort;
	private static int serverPlayers;
	
	private static JSpinner inputPort;
	private static JSpinner inputPlayers;
	
	private JFrame frmMdserver;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MDServer window = new MDServer();
					window.frmMdserver.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MDServer() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMdserver = new JFrame();
		frmMdserver.setIconImage(Toolkit.getDefaultToolkit().getImage(MDServer.class.getResource("/monopolydeal_icons/server.png")));
		frmMdserver.setResizable(false);
		frmMdserver.setTitle("Servidor de Monopoly Deal");
		frmMdserver.setBounds(100, 100, 450, 325);
		frmMdserver.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frmMdserver.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Puerto");
		lblNewLabel.setBounds(10, 11, 57, 14);
		frmMdserver.getContentPane().add(lblNewLabel);
		
		inputPort = new JSpinner();
		inputPort.setModel(new SpinnerNumberModel(5824, 0, 9999, 1));
		inputPort.setBounds(77, 8, 63, 20);
		frmMdserver.getContentPane().add(inputPort);
		
		JLabel lblJugadores = new JLabel("Jugadores");
		lblJugadores.setBounds(304, 11, 63, 14);
		frmMdserver.getContentPane().add(lblJugadores);
		
		inputPlayers = new JSpinner();
		inputPlayers.setModel(new SpinnerNumberModel(2, 2, 4, 1));
		inputPlayers.setBounds(377, 8, 57, 20);
		frmMdserver.getContentPane().add(inputPlayers);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(10, 36, 424, 2);
		frmMdserver.getContentPane().add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(10, 250, 424, 2);
		frmMdserver.getContentPane().add(separator_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(10, 49, 424, 190);
		frmMdserver.getContentPane().add(scrollPane);
		
		final JTextArea consoleOutput = new JTextArea();
		consoleOutput.setFont(new Font("Consolas", Font.PLAIN, 13));
		consoleOutput.setLineWrap(true);
		consoleOutput.setEditable(false);
		consoleOutput.setBackground(Color.BLACK);
		consoleOutput.setForeground(Color.WHITE);
		scrollPane.setViewportView(consoleOutput);
		
		final JButton btnConnect = new JButton("Conectar");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnConnect.setEnabled(false);
				serverPort = (Integer) inputPort.getValue();
				serverPlayers = (Integer) inputPlayers.getValue();
				server = Server.getInstance();
				server.setPort(serverPort);
				server.setPlayers(serverPlayers);
				consoleOutput.append("> Iniciando servidor [Puerto: " + serverPort + "] ... ");
				if (server.connect()){
					consoleOutput.append("OK\n");
				} else {
					consoleOutput.append("ERROR\n");
					consoleOutput.append("> ERROR: " + server.getError() + "\n");
				}
				for (int i = 0; i < serverPlayers; i++){
					consoleOutput.append("> Buscando Jugador ... ");
					if (server.getPlayer()){
						consoleOutput.append("OK\n");
					} else {
						consoleOutput.append("ERROR\n");
						consoleOutput.append("> ERROR: " + server.getError() + "\n");
					}
				}
				consoleOutput.append("> Cerrando servidor [Puerto: " + serverPort + "] ... ");
				if (server.disconnect()){
					consoleOutput.append("OK\n");
				} else {
					consoleOutput.append("ERROR\n");
					consoleOutput.append("> ERROR: " + server.getError() + "\n");
				}
				btnConnect.setEnabled(true);
			}
		});
		btnConnect.setBounds(345, 263, 89, 23);
		frmMdserver.getContentPane().add(btnConnect);
	}
}
