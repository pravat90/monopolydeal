package monopolydeal;

import java.util.ArrayList;

final public class CartaComodin extends Carta {
	private static final long serialVersionUID = -5528456133612295074L;
	
	private ArrayList<Integer> grupos;
    
    public CartaComodin (int grupo){
    	this.grupos = new ArrayList<Integer>();
        switch (grupo){
            case MonopolyDeal.comodinCelesteCafe:
                this.nombre = "Celeste/Cafe";
                this.valor = 1;
                this.grupos.add(MonopolyDeal.grupoCeleste);
                this.grupos.add(MonopolyDeal.grupoCafe);
                break;
            case MonopolyDeal.comodinCelesteFerrocarril:
                this.nombre = "Celeste/Ferrocarril";
                this.grupos.add(MonopolyDeal.grupoCeleste);
                this.grupos.add(MonopolyDeal.grupoFerrocarril);
                this.valor = 4;
                break;
            case MonopolyDeal.comodinRosaNaranja:
                this.nombre = "Rosa/Naranja";
                this.grupos.add(MonopolyDeal.grupoRosa);
                this.grupos.add(MonopolyDeal.grupoNaranja);
                this.valor = 2;
                break;
            case MonopolyDeal.comodinRojoAmarillo:
                this.nombre = "Rojo/Amarillo";
                this.grupos.add(MonopolyDeal.grupoRojo);
                this.grupos.add(MonopolyDeal.grupoAmarillo);
                this.valor = 3;
                break;
            case MonopolyDeal.comodinAzulVerde:
                this.nombre = "Azul/Verde";
                this.grupos.add(MonopolyDeal.grupoAzul);
                this.grupos.add(MonopolyDeal.grupoVerde);
                this.valor = 4;
                break;
            case MonopolyDeal.comodinVerdeFerrocarril:
                this.nombre = "Verde/Ferrocarril";
                this.grupos.add(MonopolyDeal.grupoVerde);
                this.grupos.add(MonopolyDeal.grupoFerrocarril);
                this.valor = 4;
                break;
            case MonopolyDeal.comodinFerrocarrilServicio:
                this.nombre = "Ferrocarril/Servicio";
                this.grupos.add(MonopolyDeal.grupoFerrocarril);
                this.grupos.add(MonopolyDeal.grupoServicio);
                this.valor = 2;
                break;
            case MonopolyDeal.comodinMulti:
                this.nombre = "Multicolor";
                this.grupos.add(MonopolyDeal.grupoCafe);
                this.grupos.add(MonopolyDeal.grupoCeleste);
                this.grupos.add(MonopolyDeal.grupoRosa);
                this.grupos.add(MonopolyDeal.grupoNaranja);
                this.grupos.add(MonopolyDeal.grupoRojo);
                this.grupos.add(MonopolyDeal.grupoAmarillo);
                this.grupos.add(MonopolyDeal.grupoAzul);
                this.grupos.add(MonopolyDeal.grupoVerde);
                this.grupos.add(MonopolyDeal.grupoFerrocarril);
                this.grupos.add(MonopolyDeal.grupoServicio);
                this.valor = 0;
                break;
        }
        this.tipo = MonopolyDeal.cartaComodin;
    }
    
    @Override
    public boolean checkGrupo (int grupo){
        return this.grupos.contains(grupo);
    }
    
    @Override
    public String toString (){
    	return "Comodin " + this.nombre + " (" + this.valor + "M)";
    }
}
